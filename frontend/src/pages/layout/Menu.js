import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import { Link } from 'react-router-dom';

const Menu = () => {

    return (
        <>
            <Navbar collapseOnSelect bg="success" variant="dark" sticky="top" expand='sm'>
                <Container>
                    <Navbar.Brand href="/">React App</Navbar.Brand>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-nav' >
                        <Nav className="me-auto">
                            <Nav.Link as={Link} to="/">Home</Nav.Link>
                            <Nav.Link as={Link} to="/package">Package</Nav.Link>
                            <Nav.Link as={Link} to="/customer">Customer</Nav.Link>
                            <Nav.Link as={Link} to="/sales">Sales</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}
export default Menu