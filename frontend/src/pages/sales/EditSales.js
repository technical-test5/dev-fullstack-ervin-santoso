import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCustomer } from '../../store/customer';
import { useHistory, useParams } from 'react-router-dom';
import { fetchPackage } from '../../store/package';

const EditSales = () => {
    const {itemsCustomer}           = useSelector(state => state.customerReducer)
    const {itemsPackage}            = useSelector(state => state.packageReducer)
    const [customer, setCustomer]   = useState('');
    const [packages, setPackages]   = useState('');
    const [inv, setInv]             = useState('');
    const [invdate, setInvDate]     = useState('');
    const [amount, setAmount]       = useState('');
    const [discount, setDiscount]   = useState('');
    const [total, setTotal]         = useState('');
    const { id }                    = useParams();

    const history                   = useHistory();
    const dispatch                  = useDispatch()

    const updateSales = async (e) => {
        e.preventDefault();
        await axios.patch(`http://127.0.0.1:8000/api/sales/update/${id}`,{
            customer: customer,
            packages: packages,
            inv: inv,
            invdate:invdate,
            amount:amount,
            discount:discount,
            total:total,
        });
        history.push("/sales");
    }
 
    useEffect(() => {
        getSalesById(); // eslint-disable-next-line
    },[]);

    const getSalesById = async () => {
        const response = await axios.get(`http://127.0.0.1:8000/api/sales/${id}`);
        setCustomer(response.data.data.customer_id);
        setPackages(response.data.data.package_id);
        setInv(response.data.data.inv_number);
        setInvDate(response.data.data.inv_date);
        setAmount(response.data.data.amount);
        setDiscount(response.data.data.total_discount_amount);
        setTotal(response.data.data.total_amount);
    }

    useEffect(() => {
        dispatch(fetchCustomer());
    },[dispatch]);
    useEffect(() => {
        dispatch(fetchPackage());
    },[dispatch]);

    return (
        <>
            <Container>
                <Row className='mt-50'>
                    <Col md={12} className="mb-100">
                        <form onSubmit={ updateSales }>
                            <div className='form-group'>
                                <Form.Label>Customer</Form.Label>
                                <Form.Control as="select" aria-label="Default select example" value={ customer }  onChange={ (e) => setCustomer(e.target.value) } required>
                                    <option value="">Pilih</option>
                                    { 
                                        itemsCustomer ? ( 
                                            itemsCustomer.map((customer,index) => (
                                                <option value={customer.id} key={index}> {customer.name}</option>
                                            ))
                                        ) : ""
                                    }
                                </Form.Control>
                            </div>
                            <div className='form-group'>
                                <Form.Label>INV</Form.Label>
                                <Form.Control 
                                    type="number"
                                    className="input"
                                    value={ inv } 
                                    onChange={ (e) => setInv(e.target.value) }
                                    placeholder="INV" 
                                    required
                                    />
                            </div>
                            <div className='form-group'>
                                <Form.Label>Package</Form.Label>
                                <Form.Control as="select" aria-label="Default select example" value={ packages }  onChange={ (e) => setPackages(e.target.value) } required>
                                    <option value="">Pilih Paket</option>
                                    { 
                                        itemsPackage ? ( 
                                            itemsPackage.map((item,index) => (
                                                <option value={item.id} key={index}> {item.name}</option>
                                            ))
                                        ) : ""
                                    }
                                </Form.Control>
                            </div>
                            <div className='form-group'>
                                <Form.Label>Date</Form.Label>
                                <Form.Control 
                                    type="date"
                                    className="input"
                                    value={ invdate } 
                                    onChange={ (e) => setInvDate(e.target.value) }
                                    placeholder="Date" 
                                    required
                                    />
                           </div>
                           <div className='form-group'>
                                <Form.Label>Amount</Form.Label>
                                <Form.Control 
                                    type="number"
                                    className="input"
                                    value={ amount } 
                                    onChange={ (e) => setAmount(e.target.value) }
                                    placeholder="Amount" 
                                    required
                                    />
                            </div>
                            <div className='form-group'>
                                <Form.Label>Discount</Form.Label>
                                <Form.Control 
                                    type="number"
                                    className="input"
                                    value={ discount } 
                                    onChange={ (e) => setDiscount(e.target.value) }
                                    placeholder="Discount" 
                                    required
                                    />
                           </div>
                           <div className='form-group'>
                                <Form.Label>Total</Form.Label>
                                <Form.Control 
                                    type="number"
                                    className="input"
                                    value={ total } 
                                    onChange={ (e) => setTotal(e.target.value) }
                                    placeholder="Total" 
                                    required
                                    />
                           </div>
                          
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </form>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default EditSales