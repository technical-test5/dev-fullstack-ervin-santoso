import axios from 'axios';
import { API_URL, GET_PACKAGE } from '../../utils/constants';

export const fetchPackage = () => {
    return (dispatch) => {
        axios
        .get(API_URL + "package")
        .then(function (response) {
            setTimeout(() => { 
                dispatch({
                    type: GET_PACKAGE,
                    payload:  {
                        loading: false,
                        data:  response.data.data,
                        errorMessage: false,
                    },
                });
            },200)
        })
        .catch(function (error) {
            setTimeout(() => { 
                dispatch({
                    type: GET_PACKAGE,
                    payload: {
                        loading: false,
                        data: false,
                        errorMessage: error.message,
                    },
                });
            },200)
        });
    };
};