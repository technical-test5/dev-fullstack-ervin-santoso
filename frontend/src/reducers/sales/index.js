import { GET_SALES, DELETE_SALES } from "../../utils/constants";

const initialState = {
    itemsSales: [],
    isFetching: false,
    error: false
}

const salesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_SALES:
            return {
                ...state,
                itemsSales: action.payload.data,
                isFetching: true,
                error: action.payload.errorMessage,
            };

        case DELETE_SALES:
            return {
                ...state,
                deleteSalesResult: action.payload.data,
                deleteIsFetching: false,
                deleteError: action.payload.errorMessage,
            };

        default:
            return state;
    }
};
export default salesReducer