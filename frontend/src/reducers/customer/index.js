import { GET_CUSTOMER } from "../../utils/constants";

const initialState = {
    itemsCustomer: [],
    isFetching: false,
    error: false
}

const customerReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CUSTOMER:
            return {
                ...state,
                itemsCustomer: action.payload.data,
                isFetching: true,
                error: action.payload.errorMessage,
            };

        default:
            return state;
    }
};
export default customerReducer