import { combineReducers } from 'redux'
import customerReducer from './customer'
import packageReducer from './package'
import salesReducer from './sales'
import salesLineReducer from './salesline'

export default combineReducers({
    customerReducer,
    packageReducer,
    salesReducer,
    salesLineReducer
})