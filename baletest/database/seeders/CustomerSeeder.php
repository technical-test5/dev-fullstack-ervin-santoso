<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'name'          => 'Ervin',
            'email'         => 'ersajogja@gmail.com',
            'phone'         => '123456789',
            'address'       => 'Yogyakarta',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Santoso',
            'email'         => 'santoso@gmail.com',
            'phone'         => '11111111',
            'address'       => 'Bantul',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Hasan',
            'email'         => 'hasan@gmail.com',
            'phone'         => '08767544554',
            'address'       => 'Sleman',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Lina',
            'email'         => 'lina@gmail.com',
            'phone'         => '565454434345',
            'address'       => 'Ngawi',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Nina',
            'email'         => 'nina@gmail.com',
            'phone'         => '1231231111123',
            'address'       => 'Pacitan',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Adam',
            'email'         => 'adam@gmail.com',
            'phone'         => '867865644',
            'address'       => 'Surabaya',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Adam Jauhari',
            'email'         => 'adamjauhari@gmail.com',
            'phone'         => '121312123123',
            'address'       => 'Lampung',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Yusuf',
            'email'         => 'yusuf@gmail.com',
            'phone'         => '02719978776',
            'address'       => 'Medan',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Surya',
            'email'         => 'surya@gmail.com',
            'phone'         => '7854643453',
            'address'       => 'Malang',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Galang',
            'email'         => 'galang@gmail.com',
            'phone'         => '123123123',
            'address'       => 'Gunung Kidul',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Ima',
            'email'         => 'ima@gmail.com',
            'phone'         => '989789797',
            'address'       => 'Bantul',
            'created_at'    => Carbon::now()
        ]);
        DB::table('customers')->insert([
            'name'          => 'Nimas',
            'email'         => 'nimas@gmail.com',
            'phone'         => '989897878',
            'address'       => 'Kulonprogo',
            'created_at'    => Carbon::now()
        ]);
    }
}
