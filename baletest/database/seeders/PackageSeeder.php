<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            'name'              => 'Basic',
            'description'       => 'Ini adalah paket basic yang sedang anda pilih.',
            'amount'            => '25000',
            'created_at'        => Carbon::now()
        ]);
        DB::table('packages')->insert([
            'name'              => 'Lite',
            'description'       => 'Paket Lite ini menyediakan berbagai macam fitur lebih daripaket basic hanya saja jika ingin mendapatkan lebih segera upgrade akun anda.',
            'amount'            => '50000',
            'created_at'        => Carbon::now()
        ]);
        DB::table('packages')->insert([
            'name'              => 'Middle',
            'description'       => 'Merupakan Paket yang populer yang menjadi banyak pilihan untuk anak-anak muda. Dapatkan sekarang mumpung sedang ada promo.',
            'amount'            => '75000',
            'created_at'        => Carbon::now()
        ]);
        DB::table('packages')->insert([
            'name'              => 'Pro',
            'description'       => 'Menawarkan paket promo terbaru untuk anda. Dijamin tidak bakalan rugi menggunakannya. Garansi uang kembali 50%.',
            'amount'            => '100000',
            'created_at'        => Carbon::now()
        ]);
        DB::table('packages')->insert([
            'name'              => 'Elite',
            'description'       => 'Paket hanya tersedia jika anda sudah menjadi bagian dari komunitas kami. Sekarang join di komunitas kami sekarang juga.',
            'amount'            => '500000',
            'created_at'        => Carbon::now()
        ]);
        DB::table('packages')->insert([
            'name'              => 'Platinum',
            'description'       => 'Paket hanya tersedia jika anda sudah menjadi bagian dari komunitas kami dan menjadi member premium kami. Daftarkan diri anda untuk menjadi member premium kami sekarang juga.',
            'amount'            => '500000',
            'created_at'        => Carbon::now()
        ]);
        
    }
}
