<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoices', function (Blueprint $table) {
            $table->id(); 
            $table->unsignedBigInteger('customer_id');
            $table->integer('inv_number'); 
            $table->date('inv_date'); 
            $table->integer('amount'); 
            $table->integer('total_discount_amount'); 
            $table->integer('total_amount'); 
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_invoices');
    }
}
